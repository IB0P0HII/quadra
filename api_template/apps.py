from django.apps import AppConfig


class ApiTemplateConfig(AppConfig):
    name = 'api_template'
