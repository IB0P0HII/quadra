from unittest import TestCase
import httplib2
from quadro.settings import SITE_URL


class TestResult_form(TestCase):
    def test_result_form(self):
        HTTP = httplib2.Http()
        result, content = HTTP.request(SITE_URL, 'GET')
        self.assertEqual(result.status, 200, result.reason)
