from unittest import TestCase
from quadro.settings import SITE_URL
import httplib2


class TestInput_form(TestCase):
    def test_input_form(self):
        HTTP = httplib2.Http()
        result, content = HTTP.request(SITE_URL, 'GET')
        self.assertEqual(result.status, 200, result.reason)
