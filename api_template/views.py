from django.shortcuts import render


def input_form(request):
    return render(request, 'api_template/input_form.html')


def result_form(request):
    return render(request, 'api_template/result_form.html')
