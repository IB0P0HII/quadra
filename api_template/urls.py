from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.input_form),
    url(r'^result/', views.result_form),
]
