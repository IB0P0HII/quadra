
$(document).ready(function () {
    $("#send-request-insert").click(function () {
        AjaxSendRequestInsert();
    });
    $("#send-request-update").click(function () {
        AjaxSendRequestUpdate();
    });
})


function AjaxSendRequestInsert(){
    $.ajax({
        url: '/api/',
        type: 'POST',
        data: $("#input-form").serialize(),
        success: function (result) {
            $("#response").html(result);
        },
    })
}

function AjaxSendRequestUpdate() {
    $("#response").html('');
    $.ajax({
        url:'/api/',
        type:'GET',
        success: function (result) {
            for (item in result){
                $("#response").append('<div class="margin-top-bottom">'+ result[item].api_date+'&nbsp;'+result[item].api_text+'</div>');
            }
        }
    })
}