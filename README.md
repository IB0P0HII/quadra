Описание
--------
Сделать базовое API с методом uploadText,
принимающее переменную txt.
Сделать страницу, в которой находится поле,
содержимое которого можно передать через AJAX на API.
В админке сделать возможность удалить выбранные
полученные данные.
Сделать вторую страницу, которая через API метод
getText получает список хранимых строк и отображает его.
Результат залить в гит, дополнить инструкцией по развёртыванию.

Требования
----------
1. Python 3.6
2. PostgreSQL
3. ОС Linux
4. nginx
5. uWSGI
6. установленные зависимости указанные в requirements.txt

Развертывание - Подготовка окружения
------------------------------------
1. Установить ОС.
2. Установить nginx
    sudo yum -y install epel-release
    sudo yum -y install nginx
3. Установить Python 3.6
    sudo yum -y install https://centos7.iuscommunity.org/ius-release.rpm
    sudo yum -y install python36u
    sudo yum -y install python36u-pip
4. Установить PostgreSQL
    sudo rpm -ivh http://repo.postgrespro.ru/pgpro-9.6/keys/postgrespro-9.6.centos.pro.yum-1.0-1.noarch.rpm
    sudo yum -y postgrespro96-server postgrespro96-contrib postgrespro96-plperl postgrespro96-plpython postgrespro96-pltcl
5. Загрузить проект на сервер.
6. Установить gcc
7. Установка зависимостей
    Перейти в папку с проектом.
    sudo pip3.6 install -r requirements.txt

Развертывание - Настройка окружения
-----------------------------------
1. Создать пользователя и базу данных в PostgreSQL.
2. Настроить данные для подключения в файле settings
3. Установить Debug в значение False и настроить ALLOWED_HOSTS
4. В папке с пректом выполнить команду python36u manage.py collectstatic
5. Переместить папку со статикой в директорию веб сервера.
6. Настроить конфигурацию веб сервера.
7. Настроить uWSGI.
8. Перезапустить nginx
9. В папке с проэктом выполнить
    python manage.py migrate
10. Запускать командой
    uwsgi --socket 127.0.0.1:8080 --wsgi-file quadro/wsgi.py

Развертывание для тестирования
------------------------------
1. Скачать проэкт.
2. В папке с проэктом выполнить
    python manage.py migrate
3. В папке с проектом выполнить команду
    python manage.py runserver 0.0.0.0:8000

Упрощенное развертывание
------------------------
1. Загрузить проэкт на сервер.
2. Настроить Nginx на проксирование.
3. Настроить раздачу статики.
4. В папке с проэктом выполнить
    python manage.py migrate
5. В папке с проектом выполнить команду
    python manage.py runserver 8000
Не рекомендуется для produktion  среды


Тестирование
------------
Для тестирования использовалась следующая конфигурация:
1. Виртуальная машина с 512мб памяти:
а) ОС Linux (CentOS7)
б) PostgrSQL 9.6 (модификация от компании PostgreSQL pro)
2. Виртуальная машина с 512мб памяти:
а) ОС Linux (CentOS7)
б) Python 3.6
в) Nginx
г) uWSGI

Unittest's
----------
Для написания юнит тестов использовалась встроенная библиотек Unittest
httplib2 - низкоуровневый hhtp-клиент.

Прочие инструменты
------------------
1. Виртуализация на базе ОС linux Mint (Ubuntu) QEMU/KVM.
2. IDE - PyCharm CE.
3. Bitbucket.org - git репозиторий
4. Методолгия работы с git - git-flow
5. Методология разработки - придерживался TDD

Замечания
---------
1. Не корректная работа с полем data согласно спецификации HTML v5
в браузере Firefox.
