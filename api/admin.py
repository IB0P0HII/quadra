from django.contrib import admin
from api.models import ApiData


class AdminApiData(admin.ModelAdmin):
    list_display = ('api_date', 'api_text')


admin.site.register(ApiData, AdminApiData)
