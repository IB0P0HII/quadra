from rest_framework import serializers
from api.models import ApiData


class ApiSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApiData
        fields = ('api_date', 'api_text')
