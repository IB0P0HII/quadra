from unittest import TestCase
import httplib2
from quadro.settings import SITE_URL


class TestApi_root(TestCase):
    def test_api_root_get(self):
        HTTP = httplib2.Http()
        response, content = HTTP.request(SITE_URL, "GET")
        self.assertEqual(response.status, 200, response.reason)

    def test_api_root_post(self):
        self.fail()
