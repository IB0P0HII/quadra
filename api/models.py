from django.db import models


class ApiData(models.Model):
    api_date = models.DateField()
    api_text = models.TextField()

    def __str__(self):
        return self.api_text
