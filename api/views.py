from django.http import JsonResponse
from rest_framework.parsers import JSONParser
from  rest_framework import status
from api.models import ApiData
from api.serializers import ApiSerializer
from django.views.decorators.csrf import csrf_exempt


@csrf_exempt
def api_root(request):
    if request.method == 'GET':
        lst_texts = ApiData.objects.all()
        serializer = ApiSerializer(lst_texts, many=True)
        return JsonResponse(serializer.data, safe=False)
    elif request.method == 'POST':
        serializer = ApiSerializer(data=request.POST)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse('Ваше сообщение успешно добавлено', status=status.HTTP_200_OK, safe=False)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
